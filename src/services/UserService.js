const { useCollection } = require('../mongodb/MongoConfig')

const userCollection = useCollection('user')
const createUser = async (body) => {
  const insert = userCollection.then((value) => value.insertOne(body))
  return insert
}

const getUsers = async () => {
  const users = userCollection.then((value) => value.find().toArray())
  return users
}

const getUser = async (id) => {
  const user = userCollection.then((value) => value.findOne({ _id: id }))
  return user
}

const editUser = async (id, body) => {
  const { value } = userCollection.then((value) =>
    value.findOneAndUpdate({ _id: id }, body)
  )
  if (value) return value
  throw 'user not valid'
}

const deleteUser = async (id) => {
  const { modifiedCount } = userCollection.then((value) =>
    value.deleteOne({ _id: id })
  )
  if (modifiedCount > 0) return null
  throw 'user not valid'
}

module.exports = { createUser, getUsers, getUser, editUser, deleteUser }

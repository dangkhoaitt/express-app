const glob = require('glob')

module.exports = (app) => {
  glob(`${__dirname}/routes/**/*Routes.js`, {}, (err, files) => {
    if (err) throw err
    files.forEach((file) => require(file)(app))
  })
}

const UserService = require('../../services/UserService')

const createUser = async (req, res) => {
  try {
  } catch {}
}

const getUsers = async (req, res) => {
  try {
    const users = await UserService.getUsers()
    if (!users) res.status(400).send('list empty!')
    res.status(201).send(users)
  } catch (err) {
    res.status(500).send(err)
  }
}

const getUser = async (req, res) => {
  try {
    const user = await UserService.getUser(req.params.id)
    if (!user) res.status(400).send('user not define!')
    res.status(201).send(user)
  } catch (err) {
    res.status(500).send(user)
  }
}

const editUser = async (req, res) => {}

const deleteUser = async (req, res) => {}

module.exports = { createUser, getUsers, getUser, editUser, deleteUser }

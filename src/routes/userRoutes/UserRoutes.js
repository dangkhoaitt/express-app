const router = require('express').Router()
const UserController = require('../userRoutes/UserController')
const RouterConstant = require('../../constant/Routes')

module.exports = (app) => {
  router.route('/').post(UserController.createUser)
  router.route('/').get(UserController.getUsers)
  router.route('/:id').get(UserController.getUser)
  router.route('/:id').patch(UserController.editUser)
  router.route('/:id').delete(UserController.deleteUser)

  app.use(RouterConstant.USER, router)
}

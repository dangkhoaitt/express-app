const { MongoClient } = require('mongodb')

const mongoConnect = async () => {
  const uri = process.env.MONGODB_URI
  const options = { useUnifiedTopology: true }
  const dbName = 'test'
  const db = new Promise((resolve, reject) => {
    MongoClient.connect(uri, options, (err, client) => {
      if (err) reject(err)
      resolve(client.db(dbName))
    })
  })
  return db
}

const useCollection = async (collectionName) => {
  const db = await mongoConnect()
  return db.collection(collectionName)
}

module.exports = { mongoConnect, useCollection }

//const createError = require('http-errors')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const chalk = require('chalk')
const pack = require('../package')
const app = express()

mode = process.env.NODE_ENV || 'dev'
// mode can be access anywhere in the project
const config = require('config').get(mode)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
// use only when you want to see the metric related to express app
// app.use(require('express-status-monitor')())

require('dotenv').config()
require('./routes')(app)

const start = () => {
  app.listen(config.port, () => {
    console.log(chalk.yellow('.................................'))
    console.log(chalk.green(config.name))
    console.log(chalk.green(`Port:\t\t${config.port}`))
    console.log(chalk.green(`Mode:\t\t${config.mode}`))
    console.log(chalk.green(`App version:\t${pack.version}`))
    console.log(chalk.green(`database connection is established`))
    console.log(chalk.yellow('.................................'))
  })
}

const dbConnection = async () => {
  start()
}

dbConnection()
